use std::collections::LinkedList;
use std::iter::FromIterator;
use glutin_window::GlutinWindow;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::{EventSettings, Events, EventLoop};
use piston::input::{
    keyboard::Key, Button, ButtonEvent, ButtonState, RenderArgs, RenderEvent, UpdateArgs,
    UpdateEvent
};
use piston::window::WindowSettings;

#[derive(Clone, PartialEq)]
enum Direction {
    Right,
    Left,
    Up,
    Down,
}

struct Snake {
    body: LinkedList<(i32, i32)>,
    dir: Direction,
}

struct App {
    gl : GlGraphics,
    snake: Snake, 
}

// 画面の背景(緑)           [ r, g, b, a ]
static GREEN : [f32; 4] = [ 0.0, 1.0, 0.0, 1.0 ];
// 操作可能なヘビの色(赤)
static RED   : [f32; 4] = [ 1.0, 0.0, 0.0, 1.0 ];

impl App {
    fn render(&mut self, args: &RenderArgs) {
        use graphics;

        // 緑色の背景を描画します。
        self.gl.draw( args.viewport(), |_c, gl| {
            graphics::clear( GREEN, gl );
        } );

        // スネークを描画します。
        self.snake.render( &mut self.gl, args );
    }

    fn update(&mut self) {
        self.snake.update();
    }

    fn pressed(&mut self, btn: &Button) {
        // 値は clone しておきます。
        let last_direction = self.snake.dir.clone();

        self.snake.dir = match btn {
            &Button::Keyboard(Key::Up)    if last_direction != Direction::Down    => Direction::Up,
            &Button::Keyboard(Key::Down)  if last_direction != Direction::Up      => Direction::Down,
            &Button::Keyboard(Key::Left)  if last_direction != Direction::Right   => Direction::Left,
            &Button::Keyboard(Key::Right) if last_direction != Direction::Left    => Direction::Right,
            _ => last_direction,
        }
    }
}

impl Snake {
    fn render(&self, gl: &mut GlGraphics, args: &RenderArgs) {
        use graphics;

        // 正方形を作成します。
        let squares :Vec<graphics::types::Rectangle> = self
            .body
            .iter()
            .map( |&(x, y)| graphics::rectangle::square( ( x * 20 ) as f64,
                                                         ( y * 20 ) as f64,
                                                           20_f64 ) )
            .collect();

        // 自分自身を描画します。
        gl.draw(args.viewport(), |c, gl| {
            let transform = c.transform;

            // それぞれの正方形を描画します。
            squares
                .into_iter()
                .for_each( |square| graphics::rectangle( RED, square, transform, gl ) );
        } );
    }

    fn update(&mut self) {
        let mut new_head = (*self.body.front().expect( "Snake has no body" ) ).clone();
        match self.dir {
            Direction::Left  => new_head.0 -= 1,
            Direction::Right => new_head.0 += 1,
            Direction::Up    => new_head.1 -= 1,
            Direction::Down  => new_head.1 += 1,
        }

        self.body.push_front( new_head );
        self.body.pop_back().unwrap();
    }
}

fn main() {
    let opengl = OpenGL::V3_2;
    let mut window : GlutinWindow = WindowSettings::new( "Snake Game", [ 640, 400 ] )
        .graphics_api( opengl )
        .exit_on_esc( true )
        .build()
        .unwrap();

    // app の初期化
    let mut app = App {
        gl: GlGraphics::new( opengl ),
        // 初期化
        snake: Snake {
            body: LinkedList::from_iter( ( vec![ (0, 0), (0, 1) ] ).into_iter() ),
            dir: Direction::Right,
        },
    };

    let mut events = Events::new( EventSettings::new() ).ups( 8 );
    while let Some( e ) = events.next( &mut window ) {
        if let Some( args ) = e.render_args() {
            app.render( &args );
        }

        if let Some( args ) = e.button_args() {
            if args.state == ButtonState::Press {
                // キー押した:
                app.pressed( &args.button );
            }
        }

        if let Some( _args ) = e.update_args() {
            app.update();
        }
    }
}
